function checkAuth(req, res, next) {
    if (!req.user)  return res.render('noauth', {
        user: req.user
    }); // 'Not authorized'
    next();  
}

function checkAdmin(req, res, next) {
    if (!req.user) res.render('noauth', {
        user: req.user
    }); // 'Not authorized'
    else if (req.user.role !== 'admin') res.sendStatus(403); // 'Forbidden'
    else next();
}

function checkNoTeam(req, res, next) {
    if (!req.user)  return res.render('noauth', {
        user: req.user
    }); 
    else if (req.user.role !== 'noteam') res.sendStatus(403); // 'Forbidden'
    else next();  
}

function checkOwner(req, res, next) {
    if (!req.user)  return res.render('noauth', {
        user: req.user
    }); 
    else if (req.user.role !== 'productowner') res.sendStatus(403); // 'Forbidden'
    else next();  
}

function checkTeamlead(req, res, next) {
    if (!req.user)  return res.render('noauth', {
        user: req.user
    }); 
    else if (req.user.role !== 'Teamlead' || req.user.role !== 'productowner') res.sendStatus(403); // 'Forbidden'
    else next();
}
module.exports = {
    checkAuth,
    checkAdmin,
    checkNoTeam,
    checkOwner,
    checkTeamlead
};
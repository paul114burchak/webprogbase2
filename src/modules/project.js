const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds249355.mlab.com:49355/heroku_vs3qmt6j";

mongoose.connect(url);
let Schema = mongoose.Schema;

let SchemaProj = new Schema({
    title: String,
    description: String,
    repo: String,
    teamlead: { username: String, github: String },
    sprints: [{
        goal: String,
        deadline: String,
    }],
    productbacklog: { data: Buffer, contentType: String },
    commits: [{ sha: String }]
});

let proj = mongoose.model('project', SchemaProj);

function create(x) {
    let newProj = new proj(x);
    const promise = newProj.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function getById(x_id) {
    const promise = proj.findOne({ _id: x_id })
        .then(proj => { console.log('proj getted id'); return proj; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function getByTitle(x_title) {
    const promise = proj.findOne({ title: x_title })
        .then(proj => { console.log('proj getted x_username'); return proj; })
        .catch(error => { throw error + x_title; });
    return promise;
}

function update(x) {
    const promise = proj.findByIdAndUpdate(x._id,x)
    .then(user => {return user;})  
    .catch((error) => {
        throw 'Update error: ' + error;
    });
    return promise;
}


module.exports = {
    create,
    getById,
    getByTitle,
    update
};
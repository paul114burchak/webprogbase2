const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds249355.mlab.com:49355/heroku_vs3qmt6j";

mongoose.connect(url);
let Schema = mongoose.Schema;

let SchemaStory = new Schema({
    project: String,
    sprint: String,
    points: Number,
    importance: Number,
    notes: String,
    title: String,
    tasks: [{
        description: String,
        done: String,
        durationDays: Number
    }],
    done: Boolean
});

let story = mongoose.model('story', SchemaStory);

function create(x) {
    let newstory = new story(x);
    const promise = newstory.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function getBySprintAndProject(sprint,project) {
    const promise = story.find({ sprint,project })
        .then(story => { console.log('story getted project,sprint'); return story; })
        .catch(error => { throw error + project; });
    return promise;
}

function getBySprintAndProjectAndTitle(sprint,project,title) {
    const promise = story.find({ sprint,project,title })
        .then(story => { console.log('story getted project,sprint'); return story; })
        .catch(error => { throw error + project; });
    return promise;
}

function getByTitle(title) {
    const promise = story.findOne({title: title})
        .then(story => { console.log('story getted project,sprint'); return story; })
        .catch(error => { throw error + title; });
    return promise;
}

function getById(x_id) {
    const promise = story.findOne({ _id: x_id })
        .then(story => { console.log('story getted id'); return story; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function update(x) {
    const promise = story.findByIdAndUpdate(x._id,x)
    .then(user => {return user;})  
    .catch((error) => {
        throw 'Update error: ' + error;
    });
    return promise;
}

module.exports = {
    create,
    getBySprintAndProject,
    getById,
    getBySprintAndProjectAndTitle,
    getByTitle,
    update
};
const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds249355.mlab.com:49355/heroku_vs3qmt6j";

mongoose.connect(url);
let Schema = mongoose.Schema;

let SchemaUser = new Schema({
    surname: String,
    username: String,
    email: String,
    github: String,
    password: String,
    team: String,
    role: String,
    avatar:{ data: Buffer, contentType: String }
});

let user = mongoose.model('user', SchemaUser);

function create(x) {
    let newUser = new user(x);
    const promise = newUser.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function getById(x_id) {
    const promise = user.findOne({ _id: x_id })
        .then(user => { console.log('user getted id'); return user; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function getByUsername(x_username) {
    const promise = user.findOne({ username: x_username })
        .then(user => { console.log('user getted x_username'); return user; })
        .catch(error => { throw error + x_username; });
    return promise;
}

function getByGithubId(x_id) {
    const promise = user.findOne({ githubId: x_id })
        .then(user => { console.log('user getted githubid'); return user; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function get(x_username, x_password) {
    const promise = user.findOne({ username: x_username, password: x_password })
        .then(user => {
            console.log("user by password");
            return user;
        })
        .catch(error => { throw error + x_username; });
    return promise;
}

function getAll() {
    const promise = user.find()
        .then(users => {
            return users;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
}

function update(x) {
    const promise = user.findByIdAndUpdate(x._id,x)
    .then(user => {return user;})  
    .catch((error) => {
        throw 'Update error: ' + error;
    });
    return promise;
}

module.exports = {
    create,
    get,
    getById,
    getAll,
    getByUsername,
    update,
    getByGithubId,
    user
};
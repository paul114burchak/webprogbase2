const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds249355.mlab.com:49355/heroku_vs3qmt6j";

mongoose.connect(url);
let Schema = mongoose.Schema;

let SchemaTeam = new Schema({
    title: String,
    owner: String,
    teamlead: {username: String, github: String},
    members: [{
        username: String,
        role: String
    }],
    avatar:{ data: Buffer, contentType: String },
    project: {title: String, repo: String}
});

let team = mongoose.model('team', SchemaTeam);

function create(x) {
    let newTeam = new team(x);
    const promise = newTeam.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function getById(x_id) {
    const promise = team.findOne({ _id: x_id })
        .then(team => { console.log('team getted id'); return team; })
        .catch(error => { throw error + x_id; });
    return promise;
}

function getByTitle(x_title) {
    const promise = team.findOne({ title: x_title })
        .then(team => { console.log('team getted x_title'); return team; })
        .catch(error => { throw error + x_title; });
    return promise;
}

function getTeamlead(x_title) {
    const promise = team.findOne({ title: x_title })
        .then(team => { 
            console.log('teamlead getted');
            if(team.teamlead.username === 'no' && team.teamlead.repo === 'no')
            throw "teamlead not set";
            return team.teamlead; 
        })
        .catch(error => { throw error + x_title; });
    return promise;
}

function getAll() {
    const promise = team.find()
        .then(teams => {
            return teams;
        })
        .catch((error) => {
            throw 'Get all error: ' + error;
        });
    return promise;
}

function update(x) {
    const promise = team.findByIdAndUpdate(x._id,x)
    .then(user => {return user;})  
    .catch((error) => {
        throw 'Update error: ' + error;
    });
    return promise;
}

module.exports = {
    create,
    getById,
    getByTitle,
    getAll,
    update,
    getTeamlead
};
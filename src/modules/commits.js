const mongoose = require('mongoose');

const url = "mongodb://pburchak:08101998P@ds249355.mlab.com:49355/heroku_vs3qmt6j";

mongoose.connect(url);
let Schema = mongoose.Schema;

let SchemaCommit = new Schema({
    sha: String,
    message: String,
    commiter: String,
    email: String,
    date: String,
    url: String
});

let commit = mongoose.model('commit', SchemaCommit);

function create(x) {
    let newcommit = new commit(x);
    const promise = newcommit.save()
        .then(y => {
            console.log("Created: %s", y);
            return y;
        })
        .catch(error => {
            console.log(error);
            return error;
        });
    return promise;
}

function getBySha(sha) {
    const promise = commit.find({ sha: sha })
        .then(commit => { console.log('commit getted sha'); return commit; })
        .catch(error => { throw error + sha; });
    return promise;
}

function getByCommiter(commiter) {
    const promise = commit.find({ commiter: commiter })
        .then(commit => { console.log('proj getted x_username'); return commit; })
        .catch(error => { throw error + commiter; });
    return promise;
}

module.exports = {
    create,
    getBySha,
    getByCommiter
};
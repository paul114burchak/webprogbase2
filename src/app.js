const userStorage = require('./modules/users.js');
const teamStorage = require('./modules/teams.js');
const projectStorage = require('./modules/project.js');
const storyStorage = require('./modules/stories.js');
const check = require('./modules/check.js');
const express = require('express');
const bodyParser = require('body-parser');
const fileload = require('express-fileupload');
let routerUser = require('./routes/user.js');
let routerAuth = require('./routes/auth.js');
let routerTeam = require('./routes/team.js');

const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(fileload());

app.use('/', routerAuth);
app.use('/', routerTeam);
app.use('/', routerUser);

app.get('/',
    (req, res) => {
        if (!req.user)
            res.render('index', { user: req.user });
        else
            res.redirect('/main');
    });

app.get('/create_project',
    check.checkOwner,
    (req, res) => {
        res.render('create_project', { user: req.user });
    });

app.post('/create_project',
    check.checkOwner,
    (req, res) => {
        let title = req.body.title;
        let description = req.body.description;
        let repo = req.body.repo;
        let teamlead = { username: req.user.username, github: req.user.github };
        let shaArray = []; //todo get commits
        projectStorage.create({
            title,
            description,
            repo,
            teamlead,
            "sprints": [],
            "backlog": { data: null, contentType: null },
            "commits": shaArray
        });
        teamStorage.getByTitle(req.user.team)
            .then(team => {
                team.project = { title, repo };
                team.teamlead = teamlead;
                teamStorage.update(team);
                res.redirect("/sprints/" + title);
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.post('/add_sprint/:title',
    check.checkAuth,
    (req, res) => {
        let goal = req.body.goal;
        let deadline = req.body.deadline;
        let title = req.params.title;
        projectStorage.getByTitle(req.params.title)
            .then(project => {
                project.sprints.push({ goal, deadline });
                projectStorage.update(project);
                res.redirect('/sprints/' + title);
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.post('/add_story/:title',
    check.checkAuth,
    (req, res) => {
        let sprint = req.body.sprint;
        let importance = req.body.imp;
        let points = req.body.points;
        let notes = req.body.notes;
        let title = req.body.title;
        storyStorage.create({
            project: req.params.title,
            sprint,
            points,
            importance,
            notes,
            title,
            tasks: [],
            done: false
        })
            .then(() => {
                res.redirect('/sprints/' + req.params.title);
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

    app.post('/add_task/:title/',
    check.checkAuth,
    (req, res) => {//elrmvjelrmgjremgbjermngjlerngjlerngjlerngjlerngjlerngjlerngjelrvmejrv
        let days = req.body.days;
        let desc = req.body.desc;
        storyStorage.getByTitle(req.params.title)
            .then(story => {
                console.log(story);
                story.tasks.push({desc,done:"no",days});
                story.update(story);
                res.redirect('/sprints/' + req.params.title + '/' + 1);
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/add_sprint/:title',
    check.checkAuth,
    (req, res) => {
        res.render('add_sprint', { user: req.user, title: req.params.title });
    });

app.get('/main',
    check.checkAuth,
    (req, res) => {
        teamStorage.getByTitle(req.user.team)
            .then(team => {
                res.render('main', { user: req.user, team });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/commits/:title',
    check.checkAuth,
    (req, res) => {
        //let repo = team.project.repo;
        // let repo = "lab8";
        // const options = {
        //     host: "api.github.com",
        //     path: "/repos/" + req.user.github + "/" + repo + "/commits",
        //     headers: { 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36' },
        //     family: 4
        // };
        // http.get(options,
        //     (result) => {
        //         result.on('data', (data) => {
        //             let commit = JSON.parse(data)[0].commit.message;
        //             console.log(commit);
        //         });
        //     });
        projectStorage.getByTitle(req.params.title)
            .then(project => {
                res.render('commits', { user: req.user, project });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/lectures',
    check.checkAuth,
    (req, res) => {
        teamStorage.getByTitle(req.user.team)
            .then(team => {
                res.render('lectures', { user: req.user, team });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/files',
    check.checkAuth,
    (req, res) => {
        teamStorage.getByTitle(req.user.team)
            .then(team => {
                res.render('files', { user: req.user, team });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/tasks/:title/:sprint',
    check.checkAuth,
    (req, res) => {
        projectStorage.getByTitle(req.params.title)
            .then(project => {
                console.log(parseInt(req.params.sprint));console.log(req.params.title);
                storyStorage.getBySprintAndProject(parseInt(req.params.sprint),req.params.title)
                    .then(stories => {
                        console.log(stories);
                        res.render('board', { user: req.user, project, stories });
                    });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.get('/sprints/:title',
    check.checkAuth,
    (req, res) => {
        // teamStorage.getByTitle(req.user.team)
        //     .then(team => {
        projectStorage.getByTitle(req.params.title)
            .then(project => {
                console.log(project);
                res.render('sprints', { user: req.user, project });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

app.listen(process.env.PORT || 5000);
const teamStorage = require('../modules/teams.js');
const userStorage = require('../modules/users.js');
const check = require('../modules/check.js');
const express = require('express');
const bodyParser = require('body-parser');
const fileload = require('express-fileupload');

let router = express.Router();

router.use(express.static('public'));
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(fileload());

router.get('/create_team',
    check.checkNoTeam,
    (req, res) => {
        userStorage.getAll()
            .then(users => {
                let message = "";
                res.render('create_team', { user: req.user, users ,message});
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

router.get('/join_team',
    check.checkNoTeam,
    (req, res) => {
        teamStorage.getAll()
            .then(teams => {
                // function Username(element, index, array){
                //     return array[index].username === req.user.username;
                // }
                // function Members(element, index, array) {
                //     if(array[index].members.find(Username))
                //     return true;
                //     return false;
                // }
                // let invTeams = teams.filter(Members);
                let invTeams = [];
                for (let i = 0; i < teams.length; i++)
                    for (let j = 0; j < teams[i].members.length; j++)
                        if (teams[i].members[j].username === req.user.username) {
                            invTeams.push(teams[i]);
                            break;
                        }
                res.render('join_team', { user: req.user, invTeams });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });

    });

router.get('/join/:title',
    check.checkNoTeam,
    (req, res) => {
        let title = req.params.title;
        req.user.role = "no_role";
        req.user.team = title;
        userStorage.update(req.user)
            .then(() => {
                teamStorage.getByTitle(title)
                    .then(team => {
                        for (let i = 0; i < team.members.length; i++)
                            if (team.members[i].username === req.user.username) {
                                team.members[i].role = "no_role_confirmed";
                                break;
                            }
                        teamStorage.update(team);
                    });
            })
            .then(() => res.redirect('/main'))
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

router.get('/update_userrole/:title',
    check.checkOwner,
    (req, res) => {
        let username = req.query.username;
        let role = req.query.role;
        let title = req.params.title;
        userStorage.getByUsername(username)
            .then(user => {
                user.role = role;
                userStorage.update(user);
            })
            .then(() => {
                teamStorage.getByTitle(title)
                    .then(team => {
                        // function Username(element, index, array) {
                        //     return array[index].username === username;
                        // }
                        // team.members.find(Username).role = role;
                        for (let i = 0; i < team.members.length; i++) {
                            if (team.members[i].username === username)
                                team.members[i].role = role;
                            if (team.members[i].role === "Teamlead")
                                userStorage.getByUsername(username)
                                    .then(user => {
                                        team.teamlead = { username: username, github: user.github };
                                    });
                            teamStorage.update(team);
                            break;
                        }
                    });
            })
            .then(() => res.redirect("/main"))
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });

module.exports = router;
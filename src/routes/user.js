const userStorage = require('../modules/users.js');
const check = require('../modules/check.js');
const crypto = require('crypto');
const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const fs = require('fs');

let router = express.Router();
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(fileUpload());
const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

router.get('/profile/:username',
    check.checkAuth,
    (req, res) => {
        userStorage.getByUsername(req.params.username)
            .then(user => {
                res.render('profile', { user });
            })
            .catch(error => {
                console.log(error);
                res.sendStatus(500);
            });
    });



router.get('/register',
    (req, res) => {
        let message = "";
        res.render('register', { user: req.user, message });
    });

router.post('/register',
    (req, res) => {
        let username = req.body.username;
        let email = req.body.email;
        let surname = req.body.surname;
        let github = req.body.github;
        let pass = req.body.pass;
        let pass2 = req.body.pass2;
        let role = 'noteam';
        let data = fs.readFileSync('./public/images/no-avatar.png');
        if (pass === pass2) {
            let userNew = {
                surname,
                username,
                email,
                github,
                "password": sha512(pass, serverSalt).passwordHash,
                role,
                "team": null,
                "avatar": { data: data, contentType: "image/png" }
            };
            userStorage.getByUsername(username)
                .then(user => {
                    if (user === null) {
                        userStorage.create(userNew)
                            .then(() => res.redirect('/'));
                    } else {
                        let message = 'Користувач з таким username вже існує';
                        res.render('register', { user: req.user, message });
                    }
                });
        } else {
            let message = 'Паролі не співпадають';
            res.render('register', { user: req.user, message });
        }
    });


module.exports = router;
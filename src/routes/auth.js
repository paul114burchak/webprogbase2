const userStorage = require('../modules/users.js');
const teamStorage = require('../modules/teams.js');
const check = require('../modules/check.js');
const bodyParser = require('body-parser');
const session = require('express-session');
const crypto = require('crypto');
const express = require('express');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');

let router = express.Router();

router.use(express.static('public'));
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());
// token 8fea5483c0063f5aaa96fac874f0470b813da5ff 
const serverSalt = "45%sAlT_";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

passport.use(new LocalStrategy(
    function (username, password, done) {
        let hash = sha512(password, serverSalt).passwordHash;
        console.log(username, hash);
        userStorage.get(username, hash)
            .then(user => {
                console.log(user);
                done(user ? null : 'Invalid username or password', user);
            });
        // .then(user => {
        //     done(null, user);
        // })
        // .catch(err => {
        //     done(err, null);
        // });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (_id, done) {
    userStorage.getById(_id)
        .then(user => {
            done(user ? null : 'No user', user);
        });
});

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/main',
        failureRedirect: '/'
    }));

router.post('/update_ava',
    check.checkAuth,
    (req, res) => {
        if (req.files.avatar) {
            console.log("cerf");
            let fname = req.files.avatar.name;
            let type = "image/" + fname.slice(fname.indexOf('.') + 1);
            let ava = { data: req.files.avatar.data, contentType: type };
            req.user.avatar = ava;
            userStorage.update(req.user)
                .then(() => {
                    res.redirect('/profile/' + req.user.username);
                })
                .catch(error => console.log(error))
                .catch(error => {
                    console.log(error);
                    res.sendStatus(500);
                });
        } else {
            res.redirect('/main');
        }
    });

router.post('/create_team',
    check.checkNoTeam,
    (req, res) => {
        if (req.body.member) {
            let fname = req.files.avatar.name;
            let type = "image/" + fname.slice(fname.indexOf('.') + 1);
            let ava = { data: req.files.avatar.data, contentType: type };
            let title = req.body.title;
            let owner = req.user.username;
            // let members = [{ username: req.body.member1, role: "no_role" },
            // { username: req.body.member2, role: "no_role" },
            // { username: req.body.member3, role: "no_role" },
            // { username: req.body.member4, role: "no_role" },
            // { username: req.body.member5, role: "no_role" }];
            let members = [];
            if(!Array.isArray(req.body.member)) {
                members.push({ username: req.body.member, role: "no_role" });
            } else {
                for(let i = 0; i < req.body.member.length;i++) {
                    members.push({ username: req.body.member[i], role: "no_role" });
                }
            }
            console.log(req.body);
            teamStorage.create({
                title,
                owner,
                "teamlead": { username: "no", github: "no" },
                members,
                "avatar": ava,
                "project": {title:null, repo:null}
            })
                .then(() => {
                    req.user.role = "productowner";
                    req.user.team = title;
                    userStorage.update(req.user)
                        .then(() => res.redirect('/main'))
                        .catch(error => {
                            console.log(error);
                            res.sendStatus(500);
                        });
                })
                .catch(error => {
                    console.log(error);
                    res.sendStatus(500);
                });
        } else {
            userStorage.getAll()
            .then(users => {
                let message = "Додайте хочаб одного учасника до команди";
                res.render('create_team', { user: req.user, users ,message});
            });
        }
    });

router.get('/logout/:id',
    check.checkAuth,
    (req, res) => {
        req.logout();
        res.redirect('/');
    });

module.exports = router;